# Patch Notes

## Compatibility
- Ready for v9.

## v9.249.3
- Chat log improvements

## v9.249.2
- New Dicefinder with 3 templates :
- Dicefinder Basic 
- Dicefinder Campaign 
- Dicefinder Dark Mode

## v9.249.1
- Text now also highlights GM secret text for all other sheets and dialogs like Journal Entries, NPC sheets, etc.
- Minimal Ui support

## v9.245.4
- Fix Combat Carousel controls icon 

## v9.245.3
- scene thumb size

## v9.245.2
- Sets the length of the navigation bar and the loading bar.

## v9.245.1
- Sidebar Macro Icon fixed

## v9.242.14
- Add GM & Owner text highlight (border) behind Game User Setting

## v9.242.13
- fix somes bugs

## v9.242.12
- Chat pop-out are readable now
- fix Combat Carousel avatar size

## v9.242.11
- adjustment of the icons size when we use Sidebar Macro Icon module 

## v9.242.10
- Chat log improvements thx @JamzTheMan

## v9.242.9
- fix Nav bar lengh again

## v9.242.8
- fix Nav bar lengh
- Whisper Chat Messages, like fast healing, are readable now

## v9.242.7
- DC Action buttons are readable now

## v9.242.6
- Improve Chat dice-roll tooltips visibility
- Fix color for dialog-content

## v9.242.5
- Change Chat Color
- Effect Area buttons are readable now
- fix somes errors

## v9.242.3
- DC Chat buttons are readable now

## v9.242.2
- missing system in module.json

## v9.242.1
- add texture for condition status overlay

## v9.238.3
- Fix PFS meter

## v9.238.2
- Sidebar Macro Icon support added thank's you @Arcanist

## v9.238.1
- First version for v9
- Thank's you @Xbozon for your work !
- Thank's you @Prolice for your help !

## v3.3.1
- New sidebar icons
- sidebar is a little more large now
- Improve Babele support thx @Xbozon

## v3.2.0
- Up Right menu panel
- Improve Monk's TokenBar support.

## v3.1.12
- Improve compatibility with Monk's tokenbar

## v3.1.11
- Navigation fix for Monk's scene Navigation

## v3.1.10
- Alternative Pause Icon fix

## v3.1.9
- improve colors on somes textures

## v3.1.8
- Chat texture changes from brown to black/grey.

## v3.1.7
- add Spanish language by @Regik
- fix error on Background

## v3.1.6
- minors bugs fixed
- New texture for Nat20
- New Background

## v3.1.5
- minors bugs fixed
- Improved Chat color text (Guys, last change, I'm tired to change that all time)
- change texture for Players list.  

## v3.1.4
- minors bugs fixed
- bug fixed with Monk's toolbars 

## v3.1.3
- minors bugs fixed

## v3.1.2
- minors updates
- add skin for Create folder

## v3.1.1
- Minors fix
- Rename Pathfinder Ui to Pathfinder Ui v3

## v3.1.0
- chat colors improved
- minimalize journal improved
- New template for Dicefinder
- somes minors bugs fixed 

## v3.0.0
### New Theme Conversion : 
- Exit Fantasy Ui by Iotech
- Welcome RPG Styled Ui by Xbozon for Pathfinder 2e
- New coin images by @JamzTheMan
- Chat-colors removed
